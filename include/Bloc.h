#ifndef BLOC_H
#define BLOC_H

#include <SFML/Graphics.hpp>

class Bloc
{
    public:
        Bloc();
        virtual ~Bloc();

        sf::IntRect getRect() { return myRect; }
        void setPosition(int i, int j);
        sf::Sprite getSprite() { return sprite; }

    private:
        sf::IntRect myRect;
        sf::Texture texture;
        sf::Sprite sprite;
};

#endif // BLOC_H
