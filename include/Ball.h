#ifndef BALL_H
#define BALL_H

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Rect.hpp>

#include "Bloc.h"

class Ball
{
    public:
        Ball();
        virtual ~Ball();

        sf::IntRect getRect() { return myRect; }

        void setPosition(int x, int y);
        sf::Sprite getSprite() { return sprite; }

        void update(float time, float arcanoidPosition);
        void collision(std::vector<Bloc*> blocks);

    private:
        sf::IntRect myRect;
        sf::Texture texture;
        sf::Sprite sprite;
        float velosity;
        sf::Vector2f direction;
};

#endif // BALL_H
