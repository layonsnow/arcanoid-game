#ifndef ARCANOID_H
#define ARCANOID_H

#include <SFML/Graphics.hpp>

class Arcanoid
{
    public:

        enum Movement {noMove = 0, left = -1, right = 1};

        Arcanoid();
        virtual ~Arcanoid();

        sf::Sprite getSprite() { return sprite; }
        float getPosition() { return _position; }
        float getSize() { return _size; }

        void update(Movement movement, float time);

    private:
        sf::Texture texture;
        sf::Sprite sprite;

        const int yPosition = 328-8;
        const float velosity = 0.0001;
        float _position;
        float _size;

};

#endif // ARCANOID_H
