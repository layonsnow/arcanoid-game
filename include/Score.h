#ifndef SCORE_H
#define SCORE_H

#include <SFML/Graphics.hpp>

class Score
{
    public:
        Score();
        virtual ~Score();

        void draw(sf::RenderWindow *window);
        void increment();

    protected:

    private:
        sf::Texture texture;
        sf::Sprite sprite1, sprite2, sprite3, sprite4;
        int score=0, scoreInc=40;
        const float width = 28.7;

};

#endif // SCORE_H
