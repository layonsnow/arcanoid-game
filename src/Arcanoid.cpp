#include "Arcanoid.h"

Arcanoid::Arcanoid()
{
    _position = 270;
    _size = 32;

    texture.loadFromFile("sprites.png", sf::IntRect(0, 120, 64, 8));
    sprite.setTexture(texture);
    sprite.setOrigin(getSize(), 0);
    sprite.setPosition(getPosition(), yPosition);
}

Arcanoid::~Arcanoid()
{
    //dtor
}

void Arcanoid::update(Movement movement, float time)
{
    _position += velosity * movement * time;

    if (_position < getSize())
        _position = getSize();

    if (_position > 540 - getSize())
        _position = 540 - getSize();

    sprite.setPosition(_position, yPosition);
}
