#include "Bloc.h"

#include <cstdlib>

Bloc::Bloc()
{
    myRect;
    texture.loadFromFile("sprite_ball.png", sf::IntRect(21, 23, 390, 313));// 411 336 241-23=218

    int random = rand()%6;

    sprite.setTexture(texture);
    sprite.setTextureRect(sf::IntRect(202*(random%2), 109*(random/2), 184, 96));
    sprite.setScale(0.25, 0.25);
}

Bloc::~Bloc()
{
    //dtor
}

void Bloc::setPosition(int i, int j)
{
    myRect.width = 46;
    myRect.height = 24;

    sprite.setPosition(40 + myRect.width*i, 30 + myRect.height*j + 20);

    myRect.left = 40 + myRect.width*i;
    myRect.top = 30 + myRect.height*j + 20;
}
