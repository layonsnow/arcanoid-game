#include "Score.h"

Score::Score()
{
    texture.loadFromFile("font.jpg", sf::IntRect(92, 396, 285, 36));
    sprite1.setTexture(texture);
    sprite2.setTexture(texture);
    sprite3.setTexture(texture);
    sprite4.setTexture(texture);



    sprite1.setTextureRect(sf::IntRect(348-92, 0, (int)width, 36));
    sprite2.setTextureRect(sf::IntRect(348-92, 0, (int)width, 36));
    sprite3.setTextureRect(sf::IntRect(348-92, 0, (int)width, 36));
    sprite4.setTextureRect(sf::IntRect(348-92, 0, (int)width, 36));
    sprite1.setPosition(500-3*(int)width, 0);
    sprite2.setPosition(500-2*(int)width, 0);
    sprite3.setPosition(500-(int)width, 0);
    sprite4.setPosition(500, 0);
}

Score::~Score()
{
    //dtor
}

void Score::draw(sf::RenderWindow *window)
{
    window->draw(sprite1);
    window->draw(sprite2);
    window->draw(sprite3);
    window->draw(sprite4);
}

void Score::increment()
{
    score += scoreInc;
    if ((score/1000)%10)
        sprite1.setTextureRect(sf::IntRect(348-92- int((10-(score/1000)%10)*width), 0, (int)width, 36));
    else
        sprite1.setTextureRect(sf::IntRect(348-92, 0, width, 36));
    if ((score/100)%10)
        sprite2.setTextureRect(sf::IntRect(348-92- int((10-(score/100)%10)*width), 0, (int)width, 36));
    else
        sprite2.setTextureRect(sf::IntRect(348-92, 0, (int)width, 36));
    if ((score/10)%10)
        sprite3.setTextureRect(sf::IntRect(348-92- int((10-(score/10)%10)*width), 0, (int)width, 36));
    else
        sprite3.setTextureRect(sf::IntRect(348-92, 0, (int)width, 36));
}
