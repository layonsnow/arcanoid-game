#include "Ball.h"

#include <math.h>

Ball::Ball()
{
    velosity = 0.00011;

    //direction = sf::Vector2f(.5*sqrt(2), -0.5*sqrt(2));
    direction = sf::Vector2f(0.6, 0.8);

    texture.loadFromFile("sprite_ball.png", sf::IntRect(612, 23, 100, 100));
    sprite.setTexture(texture);
    sprite.setScale(0.2, 0.2);
    sprite.setOrigin(50, 50);
    sprite.setPosition(270, 328-8-10);

    myRect.width=20;
    myRect.height=20;
}

Ball::~Ball()
{
    //dtor
}

void Ball::update(float time, float arcanoidPosition)
{
    sf::Vector2f position = sprite.getPosition() + direction * velosity * time;

    sprite.setPosition(position);
    if (position.x > 540 - myRect.width/2)
        if (direction.x > 0)
            direction.x = - direction.x;

    if (position.x < myRect.width/2)
        if (direction.x < 0)
            direction.x = - direction.x;

    if (position.y < myRect.width/2)
        if (direction.y < 0)
            direction.y = - direction.y;

    if (position.y > 328 - 8 - myRect.width/2 && position.y < 328 + myRect.width/2)
        if (position.x > arcanoidPosition - 32 && position.x < arcanoidPosition + 32) {
            direction = sf::Vector2f(sin(M_PI*(position.x-arcanoidPosition)/92), -cos(M_PI*(position.x-arcanoidPosition)/92));
            velosity *= 1.01;
        }

    myRect.left=position.x-myRect.width/2;
    myRect.top=position.y-myRect.width/2;
}

void Ball::collision(std::vector<Bloc*> blocks)
{
    sf::Vector2f position = sprite.getPosition();

    if (blocks.size() == 1) {
        const sf::IntRect rect = blocks.at(0)->getRect();
        if (position.x > rect.left && position.x < rect.left + rect.width)
            direction.y = -direction.y;
        else if (position.y > rect.top && position.y < rect.top + rect.height)
            direction.x = -direction.x;
        else if (position.x < rect.left && position.y < rect.top) {
            sf::Vector2f impulse = position - sf::Vector2f(rect.left, rect.top);
            float projection = (impulse.x*direction.x + impulse.y*direction.y) / (impulse.x*impulse.x + impulse.y*impulse.y);
            direction -= 2*projection*impulse;
        }
        else if (position.x < rect.left && position.y > rect.top + rect.height) {
            sf::Vector2f impulse = position - sf::Vector2f(rect.left, rect.top + rect.height);
            float projection = (impulse.x*direction.x + impulse.y*direction.y) / (impulse.x*impulse.x + impulse.y*impulse.y);
            direction -= 2*projection*impulse;
        }
        else if (position.x > rect.left + rect.width && position.y < rect.top) {
            sf::Vector2f impulse = position - sf::Vector2f(rect.left + rect.width, rect.top);
            float projection = (impulse.x*direction.x + impulse.y*direction.y) / (impulse.x*impulse.x + impulse.y*impulse.y);
            direction -= 2*projection*impulse;
        }
        else if (position.x > rect.left + rect.width && position.y > rect.top + rect.height) {
            sf::Vector2f impulse = position - sf::Vector2f(rect.left + rect.width, rect.top + rect.height);
            float projection = (impulse.x*direction.x + impulse.y*direction.y) / (impulse.x*impulse.x + impulse.y*impulse.y);
            direction -= 2*projection*impulse;
        }

        /*
        if (direction.x > 0 && position.x < rect.left)
            direction.x = - direction.x;
        if (direction.x < 0 && position.x > rect.left + rect.width)
            direction.x = - direction.x;
        if (direction.y > 0 && position.y < rect.top)
            direction.y = - direction.y;
        if (direction.y < 0 && position.y > rect.top + rect.height)
            direction.y = - direction.y;
        */
    }

    if (blocks.size() == 2) {
        if (blocks.at(0)->getRect().left == blocks.at(1)->getRect().left)
            direction.x = -direction.x;
        else if (blocks.at(0)->getRect().height == blocks.at(1)->getRect().height)
            direction.y = -direction.y;
        else {
            direction.x = -direction.x;
            direction.y = -direction.y;
        }
        return;
    }

    if (blocks.size() == 3) {
        direction.x = -direction.x;
        direction.y = -direction.y;
    }
}
