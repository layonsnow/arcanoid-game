#include <stdlib.h>
#include <SFML/Graphics.hpp>
#include <iostream>

#include "Arcanoid.h"
#include "Ball.h"
#include "Bloc.h"
#include "Score.h"

enum State {game, win, fail};

void makeBlocks(std::vector<Bloc*> blocks);

int main()
{
    State state = game;

    Score score;

    Arcanoid arcanoid;
    Ball ball;

    sf::Texture textureBackground;
    textureBackground.loadFromFile("background.jpg");
    sf::Sprite spriteBackground;
    spriteBackground.setTexture(textureBackground);
    spriteBackground.setPosition(0, 0);

    sf::Texture textureLoad;
    textureLoad.loadFromFile("loading.jpg");
    sf::Sprite spriteLoad;
    spriteLoad.setTexture(textureLoad);
    spriteLoad.setPosition(-23, -36);

    sf::Clock clock;
    sf::RenderWindow window(sf::VideoMode(540, 328), "Arcanoid");

    window.clear(sf::Color::White);
    window.draw(spriteLoad);
    window.display();

    std::vector<Bloc*> blocks;

    makeBlocks(blocks);
    for (int i=0; i<10; i++)
        for (int j=0; j<6; j++) {
            Bloc *block = new Bloc;
            block->setPosition(i, j);
            blocks.push_back(block);
    }

    clock.restart();

    while (window.isOpen())
    {
        float time = clock.restart().asMicroseconds();
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        Arcanoid::Movement movement = Arcanoid::noMove;

        std::vector<Bloc*> collBlocks;

        switch (state) {
        case game:

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) &&
                !sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
                movement = Arcanoid::left;

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) &&
                !sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
                movement = Arcanoid::right;

            arcanoid.update(movement, time);
            ball.update(time, arcanoid.getPosition());

            for (int i=0; i<blocks.size(); i++)
                if (blocks[i]->getRect().intersects(ball.getRect())) {
                    Bloc *block = blocks[i];
                    blocks.erase(blocks.begin()+(i--));
                    collBlocks.push_back(block);
                }

            ball.collision(collBlocks);

            while (collBlocks.size()) {
                Bloc *curr = collBlocks[0];
                collBlocks.erase(collBlocks.begin());
                delete curr;
                score.increment();
            }

            if (ball.getRect().top > 328)
                ;

            window.clear(sf::Color::Black);

            window.draw(spriteBackground);

            score.draw(&window);

            for (int i=0; i<blocks.size(); i++)
                window.draw(blocks.at(i)->getSprite());

            window.draw(arcanoid.getSprite());
            window.draw(ball.getSprite());

            break;
        case win:
            window.clear(sf::Color::Black);

            window.draw(spriteBackground);

            score.draw(&window);


        }

        window.display();
    }

    for (int i=0; i<blocks.size(); i++)
        delete blocks[i];

    return 0;
}

void makeBlocks(std::vector<Bloc*> blocks)
{
    for (int i=0; i<blocks.size(); i++)
        delete blocks[i];

    blocks.clear();

    for (int i=0; i<10; i++)
        for (int j=0; j<6; j++) {
            Bloc *block = new Bloc;
            block->setPosition(i, j);
            blocks.push_back(block);
    }
}
